#!/usr/bin/env python

# ---------------------------------------------------------------\
#This reducer code will input a line of text and \
#    output <word, total-count>\
# ---------------------------------------------------------------\
import sys

last_key      = None              #initialize these variables\
running_total = 0
this_key = None
abc_found = False
# -----------------------------------\
# Loop thru file\
#  --------------------------------\
for input_line in sys.stdin:
    input_line = input_line.strip()

    # --------------------------------\
    # Get Next Word    # --------------------------------\
    this_key, value = input_line.split("\t")  #the Hadoop default is tab separates key value\
                          #the split command returns a list of strings, in this case into 2 variables\
    
    #if(value.isdigit()):
    #    value = int(value)           #int() will convert a string to integer (this program does no error checking)\
    #else:
    #    abc_found = True
    # ---------------------------------\
    # Key Check part\
    #    if this current key is same \
    #          as the last one Consolidate\
    #    otherwise  Emit\
    # ---------------------------------\
    if last_key == this_key:     #check if key has changed ('==' is logical equalilty check\
        if(value != "ABC"):    
            running_total += int(value)   # add value to running total\

    else:
        if last_key and abc_found == True:             #if this key that was just read in\
                                 	       #   is different, and the previous \
                                 	       #   (ie last) key is not empy,\
                                               #   then output \
                                               #   the previous <key running-count>\
            print( "{0}\t{1}".format(last_key, running_total) )
                                 # hadoop expects tab(ie '\\t') \
                                 #    separation\
        running_total = 0        #reset values\
        #last_key = this_key
        abc_found = False
        if(value != "ABC"):
            running_total = int(value)

    if(value == "ABC"):
        abc_found = True  
    last_key = this_key  

#if last_key == this_key and abc_found:
#    print( "{0}\t{1}".format(last_key, running_total)) 