#load datasets
fileA = sc.textFile("user/cloudera/input/join1_FileA.txt")

fileA.collect()

fileB = sc.textFile("user/cloudera/input/join1_FileB.txt")

fileB.collect()

#mapper for fileA

def split_fileA(line):
	word, count = line.split(",")
	count = int(count)
	return (word, count)

fileA_data = fileA.map(split_fileA)

fileA_data.collect()

#mapper for fileB

def split_fileB(line):
	date_word, count = line.split(",")
	date, word = date_word.split()
	return (word, date + " " + count)

fileB_data = fileB.map(split_fileB)
fileB_data.collect()

#join fileB and fileA

fileB_joined_fileA = fileB_data.join(fileA_data)

fileB_joined_fileA.collect()