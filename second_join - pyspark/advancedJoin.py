#Read show files

show_views_file = sc.textFile("input/join2_gennum?.txt")

#Parse show files
def split_show_views(line):
	show, views = line.split(",")
	views = int(views)
	return (show, views)

show_views = show_views_file.map(split_show_views)

#Read channel files

show_channel_file = sc.textFile("input/join2_genchan?.txt")

#Pars channel files
def split_show_channel(line):
	show, channel = line.split(",")
	return (show, channel)

show_channel = show_channel_file.map(split_show_channel)

#Join the 2 datasets
joined_dataset = show_views.join(show_channel)

#Extract channel as key
def extract_channel_views(show_views_channel):
	channel = show_views_channel[1][1]
	views = show_views_channel[1][0]
	return (channel, views)

channel_views = joined_dataset.map(extract_channel_views)

#Sum accross all channels
def some_function(a, b):
	return a + b

result = channel_views.reduceByKey(some_function)
result.collect()